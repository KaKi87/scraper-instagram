import axios from 'axios';
import numbro from 'numbro';
import {
    decode as decodeHtmlEntities
} from 'html-entities';

const defaultClientConfig = {
    baseURL: 'https://www.instagram.com/'
};

let client;

client = axios.create(defaultClientConfig);

/**
 * Set axios client config, e.g. adapters for Deno & Tauri
 * @param {import('axios').AxiosRequestConfig} clientConfig
 * @returns {import('axios').AxiosInstance}
 */
export const setClientConfig = clientConfig => client = axios.create({
    ...defaultClientConfig,
    ...clientConfig
});

/**
 * @typedef PartialProfile
 * @property {number} followerCount
 * @property {number} followingCount
 * @property {number} postCount
 * @property {string} name
 * @property {string} id
 * @property {string} pictureUrl
 */
/**
 * Get a partial profile, same as visiting in browser without JS.
 * @param {string} username
 * @returns {Promise<PartialProfile>}
 */
export const getPartialProfile = async ({
    username
}) => {
    const
        { data } = await client(`/${username}/`),
        [
            , followerCount
            , followingCount
            , postCount
            , name
        ] = data.split('description" content="')[1].split(' (&#064;')[0].match(/^(.+) Followers, (.+) Following, (.+) Posts - See Instagram photos and videos from (.+)$/),
        {
            'props': {
                id,
                'profile_pic_url': pictureUrl
            }
        } = JSON.parse(data.split('"rootView":')[1].split(',"tracePolicy')[0]);
    return {
        followerCount: numbro.unformat(followerCount.toLowerCase()),
        followingCount: numbro.unformat(followingCount.toLowerCase()),
        postCount: numbro.unformat(postCount.toLowerCase()),
        name: decodeHtmlEntities(name),
        id,
        pictureUrl
    };
};

/**
 * @typedef PostContentType
 * @type {'image'|'video'}
 */
/**
 * @typedef ProfilePostContent
 * @property {PostContentType} type
 * @property {string} url
 */
/**
 * @typedef ProfilePost
 * @property {string} id
 * @property {string} shortcode
 * @property {number} viewCount
 * @property {string} caption
 * @property {number} commentCount
 * @property {number} timestamp
 * @property {number} likeCount
 * @property {string} thumbnailUrl
 * @property {ProfilePostContent[]} contents
 */
/**
 * @typedef RelatedProfile
 * @property {string} id
 * @property {string} name
 * @property {boolean} isPrivate
 * @property {boolean} isVerified
 * @property {string} pictureUrl
 * @property {string} username
 */
/**
 * @typedef Profile
 * @property {string} biography
 * @property {string} externalUrl
 * @property {number} followerCount
 * @property {string} facebookId
 * @property {number} followingCount
 * @property {string} name
 * @property {string} id
 * @property {string} businessCategory
 * @property {string} category
 * @property {boolean} isPrivate
 * @property {boolean} isVerified
 * @property {string} pictureUrl
 * @property {number} postCount
 * @property {ProfilePost[]} posts
 * @property {RelatedProfile[]} relatedProfiles
 */
/**
 * Get a profile, same as visiting in browser.
 * @param {string} username
 * @returns {Promise<Profile>}
 */
export const getProfile = async ({
    username
}) => {
    const
        { data } = await client({
            url: 'https://i.instagram.com/api/v1/users/web_profile_info/',
            params: {
                username
            },
            headers: {
                'x-ig-app-id': (await client(`/${username}/`)).data.split('"APP_ID":"')[1].split('"')[0]
            }
        });
    return Object.defineProperty(
        {
            biography: data['data']['user']['biography'],
            externalUrl: data['data']['user']['external_url'],
            followerCount: data['data']['user']['edge_followed_by']['count'],
            facebookId: data['data']['user']['fbid'],
            followingCount: data['data']['user']['edge_follow']['count'],
            name: data['data']['user']['full_name'],
            id: data['data']['user']['id'],
            businessCategory: data['data']['user']['business_category_name'],
            category: data['data']['user']['category_name'],
            isPrivate: data['data']['user']['is_private'],
            isVerified: data['data']['user']['is_verified'],
            pictureUrl: data['data']['user']['profile_pic_url_hd'],
            postCount: data['data']['user']['edge_owner_to_timeline_media']['count'],
            posts: data['data']['user']['edge_owner_to_timeline_media']['edges'].map(data => ({
                id: data['node']['id'],
                shortcode: data['node']['shortcode'],
                viewCount: data['node']['video_view_count'],
                caption: data['node']['edge_media_to_caption']['edges'][0]?.['node']['text'],
                commentCount: data['node']['edge_media_to_comment']['count'],
                timestamp: data['node']['taken_at_timestamp'],
                likeCount: data['node']['edge_liked_by']['count'],
                thumbnailUrl: data['node']['thumbnail_src'],
                contents: (
                    data['node']['__typename'] === 'GraphSidecar'
                        ? data['node']['edge_sidecar_to_children']['edges']
                        : [data]
                ).map(data => ({
                    'GraphImage': {
                        type: 'image',
                        url: data['node']['display_url']
                    },
                    'GraphVideo': {
                        type: 'video',
                        url: data['node']['video_url']
                    }
                }[data['node']['__typename']]))
            })),
            relatedProfiles: data['data']['user']['edge_related_profiles']['edges'].map(data => ({
                id: data['node']['id'],
                name: data['node']['full_name'],
                isPrivate: data['node']['is_private'],
                isVerified: data['node']['is_verified'],
                pictureUrl: data['node']['profile_pic_url'],
                username: data['node']['username']
            }))
        },
        '_raw',
        {
            value: data['data']['user'],
            enumerable: false
        }
    );
};

/**
 * @typedef PartialPostAuthor
 * @property {string} username
 * @property {string} id
 * @property {string} name
 * @property {number} postCount
 */
/**
 * @typedef PartialPost
 * @property {PartialPostAuthor} author
 * @property {string} thumbnailUrl
 * @property {string} caption
 * @property {string} id
 */
/**
 * Get a partial post, same as visiting in browser without JS.
 * @param shortcode
 * @returns {Promise<PartialPost>}
 */
export const getPartialPost = async ({
    shortcode
}) => {
    const
        { data } = await client(`/p/${shortcode}/`),
        [
            , authorName
            , caption
            , authorPostCount
        ] = data
            .split('og:description" content="')[1]
            .split('"')[0]
            .match(/(.+) shared a post on Instagram: &quot;([\S\s]+)&quot;. Follow their account to see (.+) posts./);
    return {
        author: {
            username: data.split('(&#064;')[1].split(') &#x2022;')[0],
            id: data
                .split('instapp:owner_user_id" content="')[1]
                .split('"')[0],
            name: authorName,
            postCount: numbro.unformat(authorPostCount.toLowerCase())
        },
        thumbnailUrl: decodeHtmlEntities(
            data
                .split('og:image" content="')[1]
                .split('"')[0]
        ),
        caption: decodeHtmlEntities(caption),
        id: data.split('media_id":"')[1].split('"')[0]
    };
};

/**
 * @typedef PostAuthor
 * @property {string} id
 * @property {boolean} isVerified
 * @property {string} pictureUrl
 * @property {string} username
 * @property {string} name
 * @property {number} postCount
 * @property {number} followerCount
 */
/**
 * @typedef PostContentTaggedProfile
 * @property {string} name
 * @property {string} id
 * @property {boolean} isVerified
 * @property {string} pictureUrl
 * @property {string} username
 * @property {number} positionX
 * @property {number} positionY
 */
/**
 * @typedef PostContent
 * @property {PostContentType} type
 * @property {string} url
 * @property {PostContentTaggedProfile[]} [taggedProfiles] - Only available for videos.
 * @property {string} [thumbnailUrl] - Only available for videos.
 * @property {number} [viewCount] - Only available for videos.
 */
/**
 * Get a post, same as visiting in browser.
 * @param shortcode
 * @returns {Promise<{contents: *[], author: {isVerified, pictureUrl, name, postCount, id, followerCount, username}, caption: *, likeCount, id, thumbnailUrl, commentCount, timestamp: number}>}
 */
export const getPost = async ({
    shortcode
}) => {
    const
        { data } = await client(`/p/${shortcode}/`),
        [, data2] = data.match(/("https:\\\/\\\/static\.cdninstagram\.com\\\/rsrc\.php\\\/v3.{5}\\\/.{2}\\\/.\\\/en_US\\\/.{11}\.js\?.+?"),"refs":\["htmlStart","tierOne","tierTwo","tierThree"]}/),
        { data: data3 } = await client(JSON.parse(data2)),
        [, data4] = data3.match(/\(function\(a,b,c,d,e,f,g,h\){"use strict";var i="(.{32})",j=".{32}",k=".{32}";/),
        { data: data5 } = await client(
            '/graphql/query/',
            {
                params: {
                    'query_hash': data4,
                    'variables': JSON.stringify({
                        shortcode
                    })
                }
            }
        );
    return {
        id: data5['data']['shortcode_media']['id'],
        thumbnailUrl: data5['data']['shortcode_media']['display_url'],
        caption: data5['data']['shortcode_media']['edge_media_to_caption']['edges'][0]['node']['text'],
        commentCount: data5['data']['shortcode_media']['edge_media_to_comment']['count'],
        timestamp: data5['data']['shortcode_media']['taken_at_timestamp'] * 1000,
        likeCount: data5['data']['shortcode_media']['edge_media_preview_like']['count'],
        author: {
            id: data5['data']['shortcode_media']['owner']['id'],
            isVerified: data5['data']['shortcode_media']['owner']['is_verified'],
            pictureUrl: data5['data']['shortcode_media']['owner']['profile_pic_url'],
            username: data5['data']['shortcode_media']['owner']['username'],
            name: data5['data']['shortcode_media']['owner']['full_name'],
            postCount: data5['data']['shortcode_media']['owner']['edge_owner_to_timeline_media']['count'],
            followerCount: data5['data']['shortcode_media']['owner']['edge_followed_by']['count']
        },
        contents: (
            data5['data']['shortcode_media']['__typename'] === 'GraphSidecar'
                ? data5['data']['shortcode_media']['edge_sidecar_to_children']['edges']
                : [data5['data']['shortcode_media']]
        ).map(data => ({
            ...{
                'GraphImage': {
                    type: 'image',
                    url: (data['node'] || data)['display_url'],
                    taggedProfiles: (data['node'] || data)['edge_media_to_tagged_user']['edges'].map(data => ({
                        name: data['node']['user']['full_name'],
                        id: data['node']['user']['id'],
                        isVerified: data['node']['user']['is_verified'],
                        pictureUrl: data['node']['user']['profile_pic_url'],
                        username: data['node']['user']['username'],
                        positionX: data['node']['x'],
                        positionY: data['node']['y']
                    }))
                },
                'GraphVideo': {
                    type: 'video',
                    url: (data['node'] || data)['video_url'],
                    thumbnailUrl: (data['node'] || data)['display_url'],
                    viewCount: (data['node'] || data)['video_view_count']
                }
            }[(data['node'] || data)['__typename']]
        }))
    };
};